import Vue from 'vue'
import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'
import { registerSW } from 'virtual:pwa-register'

import colors from 'vuetify/es5/util/colors'

import { getSettings, saveSettings } from './lib/settings'
import App from './App.vue'
import Vuetify, { VList } from 'vuetify/lib'

import '@mdi/font/css/materialdesignicons.css'

import { version } from '../package.json'

Vue.use(Vuetify)

Vue.config.productionTip = false

registerSW()

const settings = getSettings()

const app = new Vue({
  data: {
    version: version,
    settings: settings
  },
  watch: {
    settings (val) {
      saveSettings(val)
    }
  },
  render: h => h(App),
  vuetify: new Vuetify({
    components: { VList },
    theme: {
      dark: settings.dark,
      themes: {
        dark: {
          primary: colors.red.darken3,
          accent: colors.red.darken4
        },
        light: {
          primary: colors.red.darken3,
          accent: colors.red.darken4
        }
      }
    }
  })
})

Sentry.init({
  dsn: 'https://0684f9db7b554291bd59ef4f39377f1f@sentry.io/5184472',
  integrations: [new Integrations.Vue({ app, attachProps: true })],
  release: version
})

app.$mount('#app')

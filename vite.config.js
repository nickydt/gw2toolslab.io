import { defineConfig } from 'vite'
import { createVuePlugin } from 'vite-plugin-vue2'
import Components from 'unplugin-vue-components/vite'
import { VuetifyResolver } from 'unplugin-vue-components/resolvers'
import { VitePWA } from 'vite-plugin-pwa'
import viteSentry from 'vite-plugin-sentry'

export default defineConfig({
  build: {
    sourcemap: true
  },
  plugins: [
    createVuePlugin(),
    Components({
      resolvers: [VuetifyResolver()]
    }),
    VitePWA({
      manifest: {
        name: 'Dailies @ gw2.tools',
        short_name: 'GW2 dailies',
        start_url: './',
        display: 'standalone',
        background_color: '#000000',
        theme_color: '#212121'
      },
      workbox: {
        runtimeCaching: [
          {
            urlPattern: /^https:\/\/fonts\.googleapis\.com/,
            handler: 'StaleWhileRevalidate',
            options: {
              cacheName: 'google-fonts-css'
            }
          },
          {
            urlPattern: /^https:\/\/fonts\.gstatic\.com/,
            handler: 'CacheFirst',
            options: {
              cacheName: 'google-fonts-otf',
              expiration: {
                maxEntries: 25,
                maxAgeSeconds: 365 * 24 * 60 * 60
              }
            }
          },
          {
            urlPattern: /^https:\/\/api\.guildwars2\.com/,
            handler: 'NetworkFirst',
            options: {
              cacheName: 'gw2-api',
              expiration: {
                maxAgeSeconds: 6 * 60 * 60
              }
            }
          }
        ]
      }
    }),
    viteSentry({
      authToken: process.env.SENTRY_AUTH_TOKEN,
      org: 'bredsky',
      project: 'gw2tools',
      setCommits: {
        auto: true
      },
      sourceMaps: {
        include: ['./dist'],
        ignore: ['node_modules'],
        urlPrefix: '~'
      }
    })
  ]
})
